#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def readData(file):
    # convert time
    headers = ["datetime", "size", "sunit", "target", "seq", "ttl", "time", "tunit"]
    data = pd.read_csv(file, sep=' ', quotechar='"',
            parse_dates=True, names=headers, header=None)
    data["datetime"] = pd.to_datetime(data["datetime"],unit="s")
    return data


def addSeverity(data, threshold):
    # extract response time
    response = data["time"].str.split('=', expand=True)
    response.columns=["clutter", "ms"]
    time = pd.to_numeric(response["ms"], errors="coerce")

    conditions = [(time >= threshold), (time < None)]
    choices = ["yellow", "green"]
    data["color"] = np.select(conditions, choices, default="red")
    data["time"] = time


def histogram(data):
    bins = np.linspace(0,1000,2000)
    plt.hist(data["time"].fillna(1000), bins, histtype="step")


def occurences(data, threshold):
    data.fillna(1000).plot(x="datetime", y="time")


if __name__ == "__main__":
    data = readData("ping_stats.csv")
    addSeverity(data, 100)
    histogram(data)
    occurences(data, 100)
    print(data[data["time"].fillna(1000) >= 300])

    plt.show()
