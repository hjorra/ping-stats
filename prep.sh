#!/bin/sh

# clean up output of command
# ping -D -c 3 -O -i 20 <domain> | grep ^\\[
# to be used as CSV file

FILE="$1"

if [ -z "$FILE" ]; then
    echo "usage: prep.sh /path/to/file"
fi

cat "$FILE" | sed -e 's/\]//' -e 's/\[//' -e 's/\(no answer yet for\)/  \"\1\"/' -e 's/\(from .*:\)/\"\1\"/' > "$FILE.csv"
